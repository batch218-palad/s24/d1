// console.log("Hello world");

// ES6 is also known as ECMAScript 2015
// ECMAScript is the standard that is used to create implementations of the language, one which is JavaScript.
// where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.);

// New features to JavaScript


console.log("ES6 Updates");
console.log("-----------");
console.log("=> Exponent Operator");


// [SECTION] Exponent Operator

// using Math Object Methods
const firstNum = Math.pow(8, 3); // 8*8*8
console.log(firstNum);

// Using the exponent operator
const secondNum = 8**3; //8^3
console.log(secondNum);


console.log("-----------");
console.log("=> Template Literals");


// [SECTION] Template Literals

/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/



let  name = "John";

// pre template literals
let message = "Hello " +name+ "! Welcome to programming!";
console.log(message)

// Strings using template literals
// uses backtincks (``) instead of ("") or ('')

message = `Hello ${name}! Welcome to programming!`
;
console.log(message);

console.log("-----------");
console.log("=> Array Destructure");

// [SECTION] Array Destructure

// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/



const fullName = ["Juan", "Dela", "Cruz"];

//Pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`)


//Array Destructuring
// variable naming for array destructuring is based on the developers choice

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you!`)

console.log("-----------");
console.log("=> Object Destructuring");

// [SECTION] Object Destructuring



const person = {
	givenName: "Jane",
	mName: "Dela",
	surName: "Cruz"
}

//pre-object destructuring
console.log(person.givenName);
console.log(person.mName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.mName} ${person.surName}! It's good to see you again`);



console.log("-----");

//object destructing

const {givenName, mName, surName} = person;

console.log(givenName);
console.log(mName);
console.log(surName);

console.log(`Hello ${givenName} ${mName} ${surName}! It's good to see you again`);




console.log("-----------");
console.log("=> Arrow Functions");

// SECTION - Arrow Functions

/*
	-compact alternative syntax to traditional functions
	-useful for code snippets where creating functions will not be reused in any other portion of code
	-this will work with "function expression."
 (s17)

 let funcExpresion = function funcName(){
	console.log("Hello from the other side")
 }
 funcExpression();

*/

/*
	syntax

	let/const variableName = (parameter) => {
		code to execute;
	}

//invocation
variableName(argument)
*/

const hello = () => {
	console.log("Hello from the other side");
}

hello();

// pre arrow function
const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student`)
});

// arrow function
students.forEach((student) =>
	console.log(`${student} is a student`)
);

/*
//syntax:

	arrayName.arrayMethod((parameter)=>
	//code to execute;
	);
	

// anonymous function - no function name
*/

console.log("-----------");
console.log("=> Implicit Return using arrow functions");


// [SECTION] Implicit Return Statement

// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even withour the return keyword;
// const add = (x, y) =>{
// 	return x + y;
// }

/* SyntaxvariableName = (parameters) => code to execute;
	let/const 

*/

const add = (x,y) => x+y;

let total = add(1,2);
console.log(total);



function addd(x,y){
	return x+y;
}

let totall = addd(1,2);
console.log(totall);


// SECTION Default Function Argument Value

// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good morning ${name}`;
console.log(greet());
console.log(greet("John"));



console.log("-----------");
console.log("=> Class=Based Object Blueprint");


// [SECTION] Class-Based Object Blueprint

// Another approach in creating an object with key and value;
// Allows creation/instantiation of object using classes as blueprints.

// -the "constructor" is a special method of a class for creating object for that class.

/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/


class Car{
	constructor(brand, name, year){
		this.brand=brand;
		this.name=name;
		this.year=year;
	}
}


const myCar = new Car();
console.log(myCar);

//reasigning value of each property

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);













